import os
import py_compile as pc
import zipfile
import tempfile

def buildWotmod(fileName):
    tempRoot = tempfile.gettempdir()
    relZip = '/res/scripts/client/gui/mods/%sc' % os.path.basename(fileName)
    outfile = tempRoot + relZip


    if not os.path.isdir(os.path.dirname(outfile)):
        os.makedirs(os.path.dirname(outfile))

    pc.compile(fileName, outfile)

    outfileZip = os.path.basename(fileName).replace('.py', '.wotmod')

    print 'mod from: ' + fileName
    print 'into ZIP: \n' + outfileZip
    print '\t' + relZip

    if os.path.isfile(outfileZip):
        os.unlink(outfileZip)
    zipf = zipfile.ZipFile(outfileZip, 'w', zipfile.ZIP_STORED)
    prev = ''
    for p in relZip.split('/')[1:]:
        prev +=  p
        if not p.endswith(".pyc"):
            prev +=  '/'
        zipf.write(tempRoot + "/" + prev, prev, compress_type=zipfile.ZIP_STORED)

    zipf.close()
    os.unlink(outfile)


buildWotmod('mod_map_attention.py')