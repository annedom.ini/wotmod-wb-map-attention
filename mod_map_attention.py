from gui import g_keyEventHandlers, g_mouseEventHandlers
import BigWorld, GUI, Math, Keys
from AvatarInputHandler import cameras
from helpers import dependency
from skeletons.gui.battle_session import IBattleSessionProvider
from chat_commands_consts import BATTLE_CHAT_COMMAND_NAMES
from PlayerEvents import g_playerEvents
from skeletons.gui.impl import IGuiLoader

class WBMapMouseAttention:
    sessionProvider = dependency.descriptor(IBattleSessionProvider)
    uiLoader = dependency.instance(IGuiLoader)

    mouseButtonRight = False
    mouseButtonLeft = False
    old_func = None
    mouseEvent = None

    def __init__(self):
        self._initListeners()
        pass

    def fireAttention(self, command):
        coll = self.testCollision(GUI.mcursor().position[0], GUI.mcursor().position[1])
        print 'attention point:', None if coll is None else coll.closestPoint
        if coll is not None:
            self.sessionProvider.shared.chatCommands.sendAttentionToPosition3D(coll.closestPoint, command)
            return True
        return False



    def testCollision(self, x, y):

        if GUI.mcursor().visible:
            direction, start = cameras.getWorldRayAndPoint(x, y)
            end = start + direction.scale(100000.0)
            print start, end, BigWorld.player().position
            return BigWorld.wg_collideSegment(BigWorld.player().spaceID, start, end, 3)
        else:
            print 'no cursor'

    def _initListeners(self):
        g_playerEvents.onAvatarBecomePlayer += self.__onAvatarBecomePlayer
        g_playerEvents.onAvatarBecomeNonPlayer += self.__onAvatarBecomeNonPlayer


    def __onAvatarBecomePlayer(self):
        g_keyEventHandlers.add(self.handleKeys)

    def __onAvatarBecomeNonPlayer(self):
        g_keyEventHandlers.remove(self.handleKeys)


    def __checkOnGUI(self):
        return False
        # screenSize = BigWorld.screenSize()
        # mx = screenSize[0] * (GUI.mcursor().position[0] + 1) / 2
        # my = screenSize[1] * (GUI.mcursor().position[1] + 1) / 2
        # res = False
        # windows = self.uiLoader.windowsManager.findWindows(lambda x: True)
        # for w in windows:
        #     if not w.isHidden():
        #         print ' '.join(map(str, [w.globalPosition, w.size, w.isHidden(), w.__class__.__name__, w.content]))
        #         if mx >= w.globalPosition[0] and mx <= w.globalPosition[0] + w.size[0] and my >= w.globalPosition[1] and my <= w.globalPosition[1] + w.size[1]:
        #             res = True
        #             break
        # return res

    def handleKeys(self, event):
        res = False
        if event.key == Keys.KEY_LEFTMOUSE and event.isKeyDown() and event.isShiftDown():
            if not self.__checkOnGUI():
                res = self.fireAttention(BATTLE_CHAT_COMMAND_NAMES.ATTENTION_TO_POSITION)
            else:
                print 'on gui'
        if event.key == Keys.KEY_RIGHTMOUSE and event.isKeyDown() and event.isShiftDown():
            res = self.fireAttention(BATTLE_CHAT_COMMAND_NAMES.SPG_AIM_AREA)

        return res


wbMapMouseAttention = WBMapMouseAttention()