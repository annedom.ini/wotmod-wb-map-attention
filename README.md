# Attention click on map *BETA*
## Description
**EN:** Mod for game World of tanks, allows set attention points by clicking on 3D space (map itself). Install mod, an holding Ctrl+Shift click on map with left or right mouse button
[Sample video](https://youtu.be/WZyb1CQKMjo?t=8022)

**RU:** Мод для игры Мир Танков, позволяет расставлять точки внимания, кликая по 3Д пространству (по самой карте). Установи мод и и кликни на карте левой или правой кнопкой мышки [Видео-пример](https://youtu.be/WZyb1CQKMjo?t=8022)

  
## Build instructions
**EN:** simply run build.py

**RU:** просто запустить build.py

## Known problems
**EN:** Mouse events do not consumed bu flash apps on screen, so you can click through minimap, players list, battle log etc

**RU:** События мышки не поглощаются флешками на экране, ты можешь кликнуть сквозь миникарту, уши, дамаг панель итд

 ## About
 Не забудь поддержать Ютуб-канал [MoD Wotbase.net](http://www.youtube.com/c/MoDWoTBasenet?sub_confirmation=1)
 